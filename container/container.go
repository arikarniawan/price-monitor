package container

import (
	"bitbucket.org/arikarniawan/price-monitor/common"
	"bitbucket.org/arikarniawan/price-monitor/price-monitor"
	"bitbucket.org/arikarniawan/price-monitor/router"
)

// Engine is the structure of container service
type Container struct {
}

// NewEngine container
func New() *Container {
	e := &Container{}
	return e
}

func (e *Container) RouteContainer(route *router.Route) *router.Route {
	route.Common = commonEngine()
	route.Frontend = frontendEngine()
	return route
}

// commonEngine
func commonEngine() *common.Controller {
	commonController := new(common.Controller)
	return commonController
}

// frontendEngine
func frontendEngine() *price_monitor.Controller {
	frontendController := new(price_monitor.Controller)
	return frontendController
}
