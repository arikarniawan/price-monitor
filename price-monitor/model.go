package price_monitor

import "bitbucket.org/arikarniawan/price-monitor/storage/model"

// UserView model
type ProductList struct {
	ID           string `json:"id" gorm:"type:varchar(35);primary_key"`
	ProductName  string `json:"product_name" gorm:"type:varchar(100)"`
	ImageUrl     string `json:"image_url" gorm:"type:varchar(150)"`
	ProductDesc  string `json:"product_desc" gorm:"type:text"`
	ProductPrice string `json:"product_price" gorm:"type:double(9,2)"`
	ProductUrl   string `json:"product_url" gorm:"type:varchar(150)"`
	model.Model
}

type ProductPrice struct {
	ID           string `json:"id" gorm:"type:varchar(35);primary_key"`
	ProductID    string `json:"product_id"`
	ProductPrice string `json:"product_price"`
	model.Model
}
