package price_monitor

import (
	"bitbucket.org/arikarniawan/price-monitor/constants"
	"encoding/json"
	"github.com/rs/xid"
	"html/template"
	"net/http"

	"github.com/gocolly/colly"
	log "github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

type IFrontend interface {
	FormInput(ctx *fasthttp.RequestCtx)
	ProductList(ctx *fasthttp.RequestCtx)
	LinkProductHandler(ctx *fasthttp.RequestCtx)
	GetProductList(ctx *fasthttp.RequestCtx)
	ShowProductDetail(ctx *fasthttp.RequestCtx)
	PriceHistory(ctx *fasthttp.RequestCtx)
}

type Controller struct {
}

var (
	templates    = template.Must(template.ParseGlob("templates/*.html"))
	info         string
	//httpResponse BodyResponse
)

func (c *Controller) FormInput(ctx *fasthttp.RequestCtx) {

	err := templates.ExecuteTemplate(ctx, "pageone.html", nil)
	if err != nil {
		log.Errorf("fail load page : %+v", err)
		ctx.Error("Internal server error", http.StatusInternalServerError)
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetContentType("text/html")
}

func (c *Controller) LinkProductHandler(ctx *fasthttp.RequestCtx) {

	if string(ctx.FormValue("linkProduct")) == "" {
		ctx.Redirect("/price-monitor/list?status="+string(constants.EmptyValue), fasthttp.StatusOK)
	}

	productList := c.getDetailProduct(string(ctx.FormValue("linkProduct")))

	guid := xid.New()
	productList.ID = guid.String()
	productList.ProductUrl = string(ctx.FormValue("linkProduct"))

	db := ConnSQL()
	var (
		num int
		pl  ProductList
	)
	status := constants.AlreadyExist
	db.Model(pl).Where("product_url = ?", productList.ProductUrl).Count(&num)
	if num == 0 {
		db.Create(&productList)
		status = constants.Success
	}

	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	ctx.Redirect("/price-monitor/list?status="+string(status), fasthttp.StatusOK)
}

type notification struct {
	Msg string
}

func (c *Controller) ProductList(ctx *fasthttp.RequestCtx) {

	switch string(ctx.FormValue("status")) {
	case "100":
		info = "Record successfully"
	case "200":
		info = "Record already exist"
	default:
		info = "Empty value"
	}
	response := notification{Msg: info}

	err := templates.ExecuteTemplate(ctx, "pagetwo.html", response)
	if err != nil {
		log.Errorf("fail load page : %+v", err)
		ctx.Error("Internal server error", http.StatusInternalServerError)
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetContentType("text/html")
}

func (c *Controller) GetProductList(ctx *fasthttp.RequestCtx) {

	productList := new([]ProductList)

	db := ConnSQL()
	db.Find(productList)
	//httpResponse.ProductList = *productList

	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	response, _ := json.Marshal(productList)

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetContentType("application/json")
	ctx.SetBody(response)
}

func (c *Controller) getDetailProduct(url string) *ProductList {

	pl := new(ProductList)
	// Instantiate default collector
	cl := colly.NewCollector(
		// Allow requests only to fabelio.com
		colly.AllowedDomains("fabelio.com"),
	)

	// Extract product details
	cl.OnHTML("head", func(e *colly.HTMLElement) {
		pl.ProductName = e.ChildAttr(`meta[property="og:title"]`, "content")
		pl.ImageUrl = e.ChildAttr(`meta[property="og:image"]`, "content")
		pl.ProductPrice = e.ChildAttr(`meta[property="product:price:amount"]`, "content")
	})

	cl.OnHTML(".product-info__description #description", func(e *colly.HTMLElement) {
		pl.ProductDesc = e.Text
	})

	err := cl.Visit(url)
	if err != nil {
		log.Error("Scraping fail with error -> %v", err)
	}

	return pl
}

func (c *Controller) ShowProductDetail(ctx *fasthttp.RequestCtx)  {

	pl := new(ProductList)

	db := ConnSQL()
	db.Where("id = ?", string(ctx.FormValue("id"))).First(&pl)
	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	err := templates.ExecuteTemplate(ctx, "pagethree.html", pl)
	if err != nil {
		log.Errorf("fail load page : %+v", err)
		ctx.Error("Internal server error", http.StatusInternalServerError)
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetContentType("text/html")
}

func (c *Controller) PriceHistory(ctx *fasthttp.RequestCtx) {

	productPrice := new([]ProductPrice)

	db := ConnSQL()
	db.Order("created_at desc").Where("product_id = ?", ctx.FormValue("id")).Find(productPrice)
	//httpResponse = *productPrice

	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	response, _ := json.Marshal(productPrice)

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetContentType("application/json")
	ctx.SetBody(response)
}