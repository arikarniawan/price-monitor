package price_monitor

import (
	"fmt"

	"bitbucket.org/arikarniawan/price-monitor/storage"

	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	config "github.com/spf13/viper"
)

func ConnSQL() *gorm.DB {

	user := config.GetString("database_default.username")
	password := config.GetString("database_default.password")
	host := config.GetString("database_default.host")
	port := config.GetInt("database_default.port")
	database := config.GetString("database_default.database")

	sql := storage.Sql{}
	db, err := sql.Connect(user, password, fmt.Sprintf("%s:%d", host, port), database)
	if err != nil {
		log.Error(err)
	}

	return db
}
