package storage

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

type Sql struct {
	DB *gorm.DB
}

func (a *Sql) Connect(user, password, host, database string) (*gorm.DB, error) {
	var err error
	a.DB, err = gorm.Open("mysql", user+":"+password+"@tcp("+host+")/"+database+"?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		return nil, err
	}

	return a.DB, err
}
