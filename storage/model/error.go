package model

type MsgErrorWithData struct {
	Errors []ErrorField `json:"errors"`
}

type ErrorField struct {
	FieldSlug string `json:"field"`
	Message   string `json:"message"`
}

type MsgErrorWithoutData struct {
	Error string `json:"error"`
}
