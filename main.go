package main

import (
	"log"
	"os"

	"bitbucket.org/arikarniawan/price-monitor/cmd"
	"bitbucket.org/arikarniawan/price-monitor/util"
)

func main() {
	command := cmd.New()
	path, err := command.GetRoot().PersistentFlags().GetString("config")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	util.SetConfig(path)
	command.Run()
}
