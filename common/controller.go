package common

import (
	"html/template"
	"net/http"

	log "github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

type ICommon interface {
	HealthCheck(ctx *fasthttp.RequestCtx)
}

type Controller struct {
}

type info struct {
	WelcomeNote string
}

func (c *Controller) HealthCheck(ctx *fasthttp.RequestCtx) {

	response := info{WelcomeNote: "Hello, I am healthy"}
	templates := template.Must(template.ParseGlob("templates/*.html"))

	err := templates.ExecuteTemplate(ctx, "healthCheck.html", response)
	if err != nil {
		log.Errorf("ExecuteTemplate : %+v", err)
		ctx.Error("Internal server error", http.StatusInternalServerError)
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetContentType("text/html")
}
