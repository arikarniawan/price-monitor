Price Monitor
===========================
This is web service to price monitor from website fabelio.com every hours.

Work Flow
===========================
1. User will input Url Product Detail.
2. Every hours background jobs will produce data to redis and consume to get data price.
3. Data product and price history has save in MariaDB


Technical Stack
===========================
1. Programming Go SDK +1.13
2. Database MariaDB
3. Redis (pub/sub to support processes background jobs)
4. Dependency Management tool -> Dep

Dependency
===========================
1. gocraft/work -> background process library
2. jasonlvhit/gocron -> cron job library
3. gocolly/colly -> crawling library

Quick Start
===========================
### 1. Go and Dep Installation

##### A. Go
1. Download Go 1.13.6 SDK from https://golang.org/dl/
2. Extract the file using command :
    ````
    tar -C /usr/local -xzf go$VERSION.$OS-$ARCH.tar.gz
    ````
3. Edit the bin in `$HOME/.profile` or `./bashrc` using text editor. then add 
    ````
    export PATH=$PATH:/usr/local/go/bin
    ````
4. Create GO workspace folder, inside the folder create the following folder :
    ````
     > src
     > pkg
     > bin
    ````
5. Set GOPATH value to path of our workspace folder.
    
    ````
    export GOPATH=$PATH_TO_YOUR_FOLDER/workspace
    ````
6. Check the configuration in system by running `go env`.

##### B. Dep
Dep is the package manager that we used on this project. Please follow the instruction here https://github.com/golang/dep

### 2. Build
To build :
    
    $ cd $GOPATH/bitbucket.org/arikarniawan
    $ dep ensure -v
    $ go install
    $
    $ mkdir logs
    $ chmod -R 777 logs
    
Configuration :

    /configuration/config.yaml
    
Database Migration

    $ price-monitoring migrate
    
### 3. Run website
To run Web Server :

    $ price-monitoring serve
    
#### Open Home Page :
    
    http://localhost:8081/price-monitor
   
### 4. Run background process

producer data every hours :

    $ price-monitoring produce
    
consumer data :

    $ price-monitoring consume

Application structure
============================
      > bitbucket.org/arikarniawan/price-monitor
        > cmd
          > actions
        > configuration
        > price-minitoring
        > templates
        > router
        > ......