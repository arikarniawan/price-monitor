package util

import (
	"path"
	"runtime"

	"bitbucket.org/arikarniawan/price-monitor/storage"

	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	config "github.com/spf13/viper"
)

// SetConfig location
func SetConfig(p string) {
	config.SetConfigName("config")
	config.SetConfigType("yaml")
	config.AddConfigPath(p)
	config.AddConfigPath("./configuration")
	//config.AddConfigPath(GetDefaultConfigPath())

	err := config.ReadInConfig()
	if err != nil {
		log.Println("config error: ", err)
	}

	config.WatchConfig()
	config.OnConfigChange(func(e fsnotify.Event) {
		log.Println("config updated, automatically reloading services....")
	})

	log.AddHook(storage.NewLogHook().
		SetFormatType(config.GetString("log.format_output")).
		SetLogLevel(config.GetInt("log.level")).
		SetRotateLog(config.GetString("log.rotate")))

	log.SetFormatter(&log.JSONFormatter{})
}

// GetDefaultConfigPath location
func GetDefaultConfigPath() string {
	_, filename, _, ok := runtime.Caller(0)

	if ok == false {
		log.Fatal("err")
	}

	filePath := path.Join(path.Dir(filename), "../configurations/")
	return filePath
}
