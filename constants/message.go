package constants

const (
	// no.hp or email is unique
	WarningAvailabilityNoHpEmail = "NoHp atau Email sudah digunakan"

	// Name is not null
	NameIsEmpty = "Nama tidak boleh kosong"

	// Email is not empty
	EmailIsEmpty = "Email tidak boleh kosong"

	// NoHp is not empty
	NoHpIsEmpty = "NoHp tidak boleh kosong"

	// Email not valid
	EmailisNotValid = "Email tidak valid"

	// Success Code
	Success = 100

	// Already Exist
	AlreadyExist = 200

	// Empty Value
	EmptyValue = 300
)
