package constants

const (
	// WriteTimeout HTTP server default value
	WriteTimeout = 5

	// ReadTimeout HTTP server default value
	ReadTimeout = 5

	// MaxConnsPerIP HTTP server default value
	MaxConnsPerIP = 500

	// MaxRequestPerConns server default value
	MaxRequestPerConns = 500

	// Concurrency server default value
	Concurrency = 100000

	// MaxKeepaliveDuration server default value
	MaxKeepaliveDuration = 5

	// httpPort default value
	HttpPort = "8081"

	// Timezone default value
	Timezone = "Asia/Jakarta"
)
