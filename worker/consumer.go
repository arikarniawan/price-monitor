package worker

import (
	"os"
	"os/signal"

	pm "bitbucket.org/arikarniawan/price-monitor/price-monitor"
	"bitbucket.org/arikarniawan/price-monitor/storage"
	"github.com/gocolly/colly"
	"github.com/gocraft/work"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
)

type Context struct {
	customerID int64
}

func Consumer() {
	pool := work.NewWorkerPool(Context{}, 10, "price_monitor_service", storage.RedisPool)

	// Map the name of jobs to handler functions
	pool.Job("crawling_page", (*Context).CrawlingPage)

	// Customize options:
	pool.JobWithOptions("export", work.JobOptions{Priority: 10, MaxFails: 1}, (*Context).Export)

	// Start processing jobs
	pool.Start()

	// Wait for a signal to quit:
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, os.Kill)
	<-signalChan

	// Stop the pool
	pool.Stop()
}

func (c *Context) CrawlingPage(job *work.Job) error {
	// Extract arguments:
	if err := job.ArgError(); err != nil {
		return err
	}

	// Go ahead and send the email...
	pp := new(pm.ProductPrice)
	guid := xid.New()
	pp.ID = guid.String()
	pp.ProductID = job.ArgString("productID")

	// Instantiate default collector
	cl := colly.NewCollector(
		// Allow requests only to fabelio.com
		colly.AllowedDomains("fabelio.com"),
	)

	// Extract product details
	cl.OnHTML("head", func(e *colly.HTMLElement) {
		pp.ProductPrice = e.ChildAttr(`meta[property="product:price:amount"]`, "content")
	})

	err := cl.Visit(job.ArgString("productPage"))
	if err != nil {
		log.Error("Scraping fail with error -> %v", err)
	}

	// insert to database
	db := pm.ConnSQL()
	db.Create(&pp)

	// update
	pl := new(pm.ProductList)
	db.Model(&pl).Where("id = ?", pp.ProductID).Update("product_price", pp.ProductPrice)

	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	return nil
}

func (c *Context) Export(job *work.Job) error {
	return nil
}
