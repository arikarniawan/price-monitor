package worker

import (
	"fmt"

	"github.com/gocraft/work"

	pm "bitbucket.org/arikarniawan/price-monitor/price-monitor"
	"bitbucket.org/arikarniawan/price-monitor/storage"
	"github.com/jasonlvhit/gocron"
	log "github.com/sirupsen/logrus"
)

// Make an enqueuer with a particular namespace
var enqueuer = work.NewEnqueuer("price_monitor_service", storage.RedisPool)

func task() {
	fmt.Println("I am running task.")

	productList := new([]pm.ProductList)

	db := pm.ConnSQL()
	db.Find(productList)

	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	for _, rec := range *productList {
		_, err := enqueuer.Enqueue("crawling_page", work.Q{"productID": rec.ID ,"productPage": rec.ProductUrl})
		if err != nil {
			log.Fatal(err)
		}
	}

}

func Producer() {
	gocron.Every(1).Hours().Do(task)

	// Start all the pending jobs
	<-gocron.Start()

	// also, you can create a new scheduler
	// to run two schedulers concurrently
	s := gocron.NewScheduler()
	s.Every(3).Seconds().Do(task)
	<-s.Start()
}
