package router

import (
	"bitbucket.org/arikarniawan/price-monitor/common"
	price_monitor "bitbucket.org/arikarniawan/price-monitor/price-monitor"
	"github.com/buaazp/fasthttprouter"
)

// Route
type Route struct {
	Common   common.ICommon
	Frontend price_monitor.IFrontend
}

// Initialize router
func (route *Route) Initialize() *fasthttprouter.Router {
	router := fasthttprouter.New()

	// healthcheck endpoint
	router.Handle("GET", "/price-monitor/healthcheck", route.Common.HealthCheck)

	// Main Page
	router.Handle("GET", "/price-monitor", route.Frontend.FormInput)
	router.Handle("POST", "/price-monitor/create", route.Frontend.LinkProductHandler)
	router.Handle("GET", "/price-monitor/list", route.Frontend.ProductList)
	router.Handle("GET", "/price-monitor/products", route.Frontend.GetProductList)
	router.Handle("GET", "/price-monitor/product-detail", route.Frontend.ShowProductDetail)
	router.Handle("GET", "/price-monitor/price-history", route.Frontend.PriceHistory)

	return router
}
