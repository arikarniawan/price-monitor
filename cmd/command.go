package cmd

import (
	"bitbucket.org/arikarniawan/price-monitor/cmd/action"
	"bitbucket.org/arikarniawan/price-monitor/container"
	"bitbucket.org/arikarniawan/price-monitor/router"
	"bitbucket.org/arikarniawan/price-monitor/worker"

	"github.com/spf13/cobra"
)

// Command cli
type Command struct {
	rootCmd *cobra.Command
}

// New command line boot loader
func New() *Command {

	var rootCmd = &cobra.Command{
		Use:   "Webservice",
		Short: "Webservice command line",
		Long:  "Webservice command line",
	}

	rootCmd.PersistentFlags().StringP("config", "c", "configuration", "the config path location")
	return &Command{
		rootCmd: rootCmd,
	}
}

// GetRoot the command line service
func (c *Command) GetRoot() *cobra.Command {
	return c.rootCmd
}

// Runs the all command line
func (c *Command) Run() {
	container := container.New()
	var rootCommands = []*cobra.Command{
		{
			Use:   "serve",
			Short: "Serve HTTP services",
			Long:  "Serve HTTP services",
			Run: func(cmd *cobra.Command, args []string) {
				action.StartServer(container.RouteContainer(new(router.Route)).Initialize())
			},
		},
		{
			Use:   "migrate",
			Short: "Database migration function",
			Long:  "Database migration function",
			Run: func(cmd *cobra.Command, args []string) {
				action.DatabaseMigration()
			},
		},
		{
			Use:   "produce",
			Short: "Produce Data",
			Long:  "Produce data for queue",
			Run: func(cmd *cobra.Command, args []string) {
				worker.Producer()
			},
		},
		{
			Use:   "consume",
			Short: "Consume Data",
			Long:  "Consume data for Crawling Page",
			Run: func(cmd *cobra.Command, args []string) {
				worker.Consumer()
			},
		},
	}

	for _, command := range rootCommands {
		c.rootCmd.AddCommand(command)
	}

	c.rootCmd.Execute()
}
