package action

import (
	"bitbucket.org/arikarniawan/price-monitor/price-monitor"
	"fmt"
)

func DatabaseMigration() {
	// migration Database
	a := price_monitor.ConnSQL()
	a.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(price_monitor.ProductList{})
	a.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(price_monitor.ProductPrice{})

	// print message
	fmt.Println("migration process done.")
}
