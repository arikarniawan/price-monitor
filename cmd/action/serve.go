package action

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/arikarniawan/price-monitor/constants"
	"bitbucket.org/arikarniawan/price-monitor/util"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/reuseport"

	config "github.com/spf13/viper"
)

type server struct {
	HTTPServer *fasthttp.Server
	router     *fasthttprouter.Router
}

func StartServer(router *fasthttprouter.Router) {
	var wg sync.WaitGroup

	// create new instance server
	server := newServer(router)

	// run server
	server.Run(&wg)
	wg.Wait()
}

// NewServer creates a new HTTP Server
func newServer(router *fasthttprouter.Router) *server {
	// create handler from router
	handler := router.Handler

	// define fasthttp server
	return &server{
		HTTPServer: newHTTPServer(handler),
		router:     router,
	}
}

// NewServer creates a new HTTP Server
// TODO: configuration should be configurable
func newHTTPServer(h fasthttp.RequestHandler) *fasthttp.Server {

	// set readtimeout from config, if failed then set default value
	readTimeout := config.GetDuration("http.read_timeout")
	if readTimeout == 0 {
		readTimeout = constants.ReadTimeout // set default value
	}

	// set writeTimeout from config, if failed then set default value
	writeTimeout := config.GetDuration("http.write_timeout")
	if writeTimeout == 0 {
		writeTimeout = constants.WriteTimeout // set default value
	}

	// set maxConnsPerIP from config, if failed then set default value
	maxConnsPerIP := config.GetInt("http.max_connections_per_ip")
	if maxConnsPerIP == 0 {
		maxConnsPerIP = constants.MaxConnsPerIP // set default value
	}

	// set maxRequestsPerConn from config, if failed then set default value
	maxRequestsPerConn := config.GetInt("http.max_request_per_connections")
	if maxRequestsPerConn == 0 {
		maxRequestsPerConn = constants.MaxRequestPerConns // set default value
	}

	// set concurrency from config, if failed then set default value
	concurrency := config.GetInt("http.max_concurrency")
	if concurrency == 0 {
		concurrency = constants.Concurrency // The maximum number of concurrent requests http may process
	}

	// set maxKeepaliveDuration from config, if failed then set default value
	maxKeepaliveDuration := config.GetDuration("http.max_keep_alive_duration")
	if maxKeepaliveDuration == 0 {
		maxKeepaliveDuration = constants.MaxKeepaliveDuration // set dafault value
	}

	return &fasthttp.Server{
		Handler:              h,
		ReadTimeout:          readTimeout * time.Second,
		WriteTimeout:         writeTimeout * time.Second,
		MaxConnsPerIP:        maxConnsPerIP,
		MaxRequestsPerConn:   maxRequestsPerConn,
		MaxKeepaliveDuration: maxKeepaliveDuration * time.Second,
		Concurrency:          concurrency,
		ReduceMemoryUsage:    true,
	}
}

// Run starts the HTTP server and performs a graceful shutdown
func (s *server) Run(wg *sync.WaitGroup) {
	var (
		port = config.GetString("http.port")
	)

	if port == "" {
		port = constants.HttpPort // set http port default value (8081)
	}

	// create a fast listener ;)
	ln, err := reuseport.Listen("tcp4", fmt.Sprintf(":%s", port))
	if err != nil {
		log.Fatalf("error in reuseport listener: %s", err)
	}

	// create a graceful shutdown listener
	duration := 5 * time.Second
	graceful := util.NewGracefulListener(ln, duration)

	// Error handling
	listenErr := make(chan error, 1)

	// Run server
	wg.Add(1)
	go func() {
		fmt.Println(fmt.Sprintf("http server successfully started, listening on port : %v", port))
		listenErr <- s.HTTPServer.Serve(graceful)
		wg.Done()
	}()

	// SIGINT/SIGTERM handling
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, syscall.SIGINT, syscall.SIGTERM)

	// Handle channels/graceful shutdown
	for {
		select {
		// If server.ListenAndServe() cannot start due to errors such
		// as "port in use" it will return an error.
		case err := <-listenErr:
			if err != nil {
				fmt.Println(fmt.Sprintf("http server failed to start, error: %s", err))
			}
			os.Exit(0)

		// handle termination signal
		case <-osSignals:
			fmt.Println("Shutdown signal received.")

			// Servers in the process of shutting down should disable KeepAlives
			s.HTTPServer.DisableKeepalive = true

			// Attempt the graceful shutdown by closing the listener
			// and completing all inflight requests.
			if err := graceful.Close(); err != nil {
				fmt.Println(fmt.Sprintf("http service gracefully stop failed, error: %v", err))
			}
		}
	}
}
